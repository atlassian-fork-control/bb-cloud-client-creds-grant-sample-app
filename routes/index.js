var express = require('express');
var router = express.Router();
var config = require('../config');
var request = require('request');

var auth_grant = {
    oauth_link: "https://bitbucket.org/account/user/bitbucket-cloud-dev/api",
    response_type: "code",
    type: {
        auth_grant_type : "Client Credentials Grant",
        definition: "<p>" +
                    "The client credentials (or other forms of client authentication) can " +
                    "be used as an authorization grant when the authorization scope is " +
                    "limited to the protected resources under the control of the client, " +
                    "or to protected resources previously arranged with the authorization " +
                    "server. " +
                    "</p>" +
                    "<p>" +
                    "Client credentials are used as an authorization grant " +
                    "typically when the client is acting on its own behalf (the client is " +
                    "also the resource owner), or is requesting access to protected " +
                    "resources based on an authorization previously arranged with the " +
                    "authorization server." +
                    "</p>",
        diff_with_other_grants: [
            "Don't need user initiation",
            "Can be used directly to obtain an access_token",
            "Uses the client's credentials to authenticate",
            "Consumers should be marked private"
        ],
        document_link : "https://developer.atlassian.com/cloud/bitbucket/oauth-2/",
        rfc_link: "https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.4"
    },
    client: {
        key: config.consumerKey,
        secret: config.consumerSecret
    }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', auth_grant);
});

router.get(`/healthcheck`, function(req, res) {
  res.send('OK');
});

/*
 * This is where the action happens! After the user authorizes the app, this endpoint processes the access_token
 * and invokes a REST API
 */
router.post('/oauth-callback', function(req, res) {
    console.log('==================================================================');
    console.log('Start building our request to get an access_token');
    console.log('==================================================================');

    let client_id_input = req.body.client_key_input;
    let client_secret_input = req.body.client_secret_input;

    console.log(`Generate basic authentication of consumer_key:consumer_secret: ${client_id_input}:${client_secret_input}`);
    let digested = new Buffer(`${client_id_input}:${client_secret_input}`).toString('base64');

    let request_fallback_strategy = (options, callback) => {
        request(options, function (error, response, body) {
            let body_json = JSON.parse(body);
            if (body_json.error != undefined) {
                console.log(error);
                res.render('request', {
                    request: {
                        method: "ERROR",
                        call: ""
                    },
                    response: {
                        body: JSON.stringify(body_json, null, 2)
                    }
                });
            }

            else {
                console.log(body);
                callback(body);
            }


        });
    };

    var callback = function (body){
        let body_json = JSON.parse(body);
        let access_token = body_json.access_token;
        console.log(`Response: \n ${JSON.stringify(body_json, null, 2)}`);
        console.log('==================================================================');
        console.log(`Callback from authorization server with access_token: ${access_token} to be used for API requests`);
        console.log('==================================================================');
        console.log(`Start building our request to GET end-user's repository with access_token : ${access_token}`);
        console.log('==================================================================');

        let options = { method: 'GET',
            url: 'https://api.bitbucket.org/2.0/repositories',
            headers:
                {
                    'Cache-Control': 'no-cache',
                    Authorization: `Bearer ${access_token}` } };

        console.log(`Request\n ${JSON.stringify(options, null, 2)}`);
        request_fallback_strategy(options, function(body_repo) {
                                                res.render('request', {
                                                    access_token: {
                                                        access_token : access_token,
                                                        request: JSON.stringify(access_token_options, null, 2),
                                                        response: JSON.stringify(body_json, null, 2)
                                                    },

                                                    repository: {
                                                        request: JSON.stringify(options, null, 2),
                                                        response: JSON.stringify(JSON.parse(body_repo), null, 2)
                                                    },
                                                    type: {
                                                        auth_grant_type: auth_grant.type.auth_grant_type
                                                    }
                                                });
                                            })
    };

    let access_token_options = { method: 'POST',
        url: 'https://bitbucket.org/site/oauth2/access_token',
        headers:
            { 'Content-Type': 'application/x-www-form-urlencoded',
                'Cache-Control': 'no-cache',
                Authorization: `Basic ${digested}`
            },
        form: { grant_type: 'client_credentials' } };


    console.log(`Request\n ${JSON.stringify(access_token_options, null, 2)}`);
    request_fallback_strategy(access_token_options, callback);
});

module.exports = router;
