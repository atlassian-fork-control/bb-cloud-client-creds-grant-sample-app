
Bitbucket Cloud Client Credentials Grant Sample App
---

## Introduction
This app aims to demonstrate the Client Credentials Grant Type Authorization flow, what is needed to generate an access_token and how to use that to invoke Bitbucket REST APIs, and lastly a reference app where the developer can copy paste the code and try it in their own projects

## What is Client Credentials Grant?
The client credentials (or other forms of client authentication) can be used as an authorization grant when the authorization scope is limited to the protected resources under the control of the client, or to protected resources previously arranged with the authorization server.
Client credentials are used as an authorization grant typically when the client is acting on its own behalf (the client is also the resource owner), or is requesting access to protected resources based on an authorization previously arranged with the authorization server.

### Difference with other grants
- Don't need user initiation
- Can be used directly to obtain an access_token
- Uses the client's credentials to authenticate
- Consumers should be marked private

### Authorization Flow Chart
![Alt text](public/images/authorization_flow.png?raw=true "Bitbucket Authorization Grant Flow")

### Things to consider
*What is OAuth?*

OAuth is a mechanism for authorizing an entity and granting them access over which the user is capable of doing in an application

*What it is not?*

OAuth is not a way to authenticate but rather to authorize an entity for access

*Definition of terms*

| Term          									| Description   |
|---------------------------------------------------|-------------|
|Basic Auth    										|Is an authentication mechanism. Is straightforward but unsecure. It is the combination of your username and password that you would use to access an API endpoint.|
|client_id     										|Key generated from Bitbucket Settings → Add Consumer   |
|secret        										|Secret generated from Bitbucket Settings → Add Consumer|
|https://bitbucket.org/site/oauth2/authorize        |URL that you use to authorize an entity|
|https://bitbucket.org/site/oauth2/access_token		|URL that generates an access token for an entity|
|access_token										|Defines the permission of the selected user, ergo represents the authorization of a specific application to access specific parts of a user's data. Tokens that represents the authorization of an entity to access specific parts of a user's data in an application. It is the token generated or returned by https://bitbucket.org/site/oauth2/access_token|

*Participants*

| Role          									| Description   |
|---------------------------------------------------|-------------|
|Resource Owner/End-user    						|An entity capable of granting access to a protected resource. When the resource owner is a person, it is referred to as an end- user.|
|Client     										|An application making protected resource requests on behalf of the resource owner and with its authorization.|
|Resource Server        							|The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens.|
|Authorization Server						       	|The server issuing access tokens to the client after successfully authenticating the resource owner and obtaining authorization.|

## Prerequisite
1. NodeJs

## Running the app
1. You are free to use the consumer key and secret configured in this project or generate your own. If you are [creating your own oauth consumer](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html#OAuthonBitbucketCloud-Createaconsumer), please make sure to set the callback URL to `http://localhost:3000/oauth-callback` as `/oauth-callback` processes the response from Bitbucket's Authorization server, just replace the consumerKey and consumerSecret in config.json and you're good to go.
2. In the project's root directory, run `npm install`. This should download all the dependencies for you needed by the app
3. Once download is complete, run `npm start`
4. Open your browser and launch: http://localhost:3000/ , this should display an intuitive page on how to go through the authorization flow

#### See it live [here](https://warm-hamlet-70857.herokuapp.com/)

## Navigating the app

| Endpoint 			| Description |
|-------------------|-------------|
| /					|Redirects the user to the landing page of the sample app |
| /healthcheck		|Test whether your app is alive							|
| /authorize		|Redirects the user for permission to access, if not yet authorized to access. Once you've granted access to your data, invoking this endpoint would get a temporary code from Bitbucket's authorization server and returned to the callback URL defined in OAuth consumer settings|
| /oauth-callback	|URL that initiates getting the access_token|

## Support

If you have any questions about this app or simply want to give us feedback please do so in the [Bitbucket Cloud category](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud) on the Developer Community.

## Related Documentation
- [Bitbucket OAuth 2.0](https://developer.atlassian.com/cloud/bitbucket/oauth-2/)
- [IETF - Client Credentials Grant](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.4)
- [OAuth Participants](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.1)
